A drupal integration of Dmitry Semenov's excellent Royal Slider which to my mind, is one of the best paid-for slider libraries out there.

http://dimsemenov.com/plugins/royal-slider/ (Demo)
http://codecanyon.net/item/royalslider-touchenabled-jquery-image-gallery/461126 (Download)

This initial version of the module turns a set of blocks in a specific region into a fixed height slider. This slider is only shown at a single path (Defaults to front page).

In addition it only makes available a small sub-set of the Royal Slider settings. If you want access to others then feel free to make a feature request.

There are a large number of 'slider' modules available. Why make another one?

I needed three features, namely:

*  Responsive layout
*  HTML block content as slides
*  Touch enabled

Royal Slider does all this and more!
Installation

*  Install Libraries API 2.x
*  Install jQuery Update
*  Download the Royal Slider library from http://codecanyon.net/item/royalslider-touchenabled-jquery-image-gallery...
*  Unzip the file and rename the folder to "royalslider" (pay attention to the case of the letters)
*  Put the folder in a libraries directory
*  Ex: sites/all/libraries
*  Ensure you have a valid path similar to this one for all files
*  Ex: sites/all/libraries/royalslider/jquery.royalslider.min.js

Configuration

Assuming you have a region which contains several blocks!

*  Go to the config page, located at: admin/config/user-interface/royal-slider
*  Select the region
*  Select the theme
*  Set the height
*  Set the height
*  Set the path
